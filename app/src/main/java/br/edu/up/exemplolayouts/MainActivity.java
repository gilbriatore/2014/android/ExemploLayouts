package br.edu.up.exemplolayouts;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageButton;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);






    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    char jogadorDaVez = 'x';
    public void jogar(View view) {

        ImageButton btn = (ImageButton) view;

        if (jogadorDaVez == 'x') {
            btn.setBackgroundResource(R.drawable.x_preto);
            jogadorDaVez = 'o';
        } else{
            btn.setBackgroundResource(R.drawable.o_preto);
            jogadorDaVez = 'x';
        }

    }

    public void limpar(View view) {

        GridLayout grid  = (GridLayout) findViewById(R.id.tabuleiro);

        ArrayList<View> lista = grid.getTouchables();
        for (View v : lista){
            ImageButton btn = (ImageButton) v;
            btn.setBackgroundResource(0);
        }
    }
}
